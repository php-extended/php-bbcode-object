<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Bbcode\BbcodeTextNode;
use PHPUnit\Framework\TestCase;

/**
 * BbcodeTextNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Bbcode\BbcodeTextNode
 *
 * @internal
 *
 * @small
 */
class BbcodeTextNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BbcodeTextNode
	 */
	protected BbcodeTextNode $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('text-string', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BbcodeTextNode('text-string');
	}
	
}
