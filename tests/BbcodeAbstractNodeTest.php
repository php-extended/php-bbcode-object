<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Bbcode\BbcodeAbstractNode;
use PHPUnit\Framework\TestCase;

/**
 * BbcodeAbstractNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Bbcode\BbcodeAbstractNode
 *
 * @internal
 *
 * @small
 */
class BbcodeAbstractNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BbcodeAbstractNode
	 */
	protected BbcodeAbstractNode $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[name=property]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $this->getMockForAbstractClass(BbcodeAbstractNode::class, ['name', 'property']);
	}
	
}
