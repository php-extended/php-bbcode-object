<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Bbcode\BbcodeCollectionNode;
use PhpExtended\Bbcode\BbcodeTextNode;
use PHPUnit\Framework\TestCase;

/**
 * BbcodeCollectionNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Bbcode\BbcodeCollectionNode
 *
 * @internal
 *
 * @small
 */
class BbcodeCollectionNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BbcodeCollectionNode
	 */
	protected BbcodeCollectionNode $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[name="property"]text-value[/name]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BbcodeCollectionNode('name', 'property', [
			new BbcodeTextNode('text-value'),
		]);
	}
	
}
