<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Bbcode\BbcodeSingleNode;
use PHPUnit\Framework\TestCase;

/**
 * BbcodeSingleNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Bbcode\BbcodeSingleNode
 *
 * @internal
 *
 * @small
 */
class BbcodeSingleNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BbcodeSingleNode
	 */
	protected BbcodeSingleNode $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('[name]', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BbcodeSingleNode('name');
	}
	
}
