<?php declare(strict_types=1);

/**
 * Test script for bbcode parser
 * 
 * This file will give the data in object tree form.
 * 
 * Usage:
 * Php test.php "<string_bbcode>"
 * 
 * @author Anastaszor
 */

use PhpExtended\Bbcode\BbcodeParser;

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException('The first argument should be the bbcode data to parse.');
}
$data = $argv[1];

echo "\n\t".'Input : '.$data."\n\n";

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
{
	throw new RuntimeException('You should run composer first.');
}
require $composer;

$parser = BbcodeParser::get();
$node = $parser->parseString($data);
var_dump($node);
