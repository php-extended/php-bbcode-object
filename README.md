# php-extended/php-bbcode-object

A library that implements the php-extended/php-bbcode-interface interface library.

![coverage](https://gitlab.com/php-extended/php-bbcode-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-bbcode-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-bbcode-object ^8`


## Basic Usage

To build a bbcode tree, just use the collection node and the single nodes :

```php

use PhpExtended\Bbcode\BbcodeCollectionNode;
use PhpExtended\Bbcode\BbcodeSingleNode;
use PhpExtended\Bbcode\BbcodeTextNode;

$_1 = new BbcodeCollectionNode('center');
$_2 = new BbcodeCollectionNode('url', 'https://example.com');
$_1->addNode($_2);
$_3 = new BbcodeSingleNode('img', 'https://example.com/toto.png');
$_2->addNode($_3);
$_4 = new BbcodeTextNode('An image of Toto');
$_2->addNode($_4);
$_1->__toString();
// echo [center][url=https://example.com][img]https://example.com/toto.png[/img]An image of Toto[/url][/center]

```

Note: the use of the `BbcodeAbstractNode::TYPE_TEXT` in a `BbcodeCollectionNode`
will disable the output of the tags in the `__toString()` method. This may
be useful to have collections that are not encapsulated in tag data, like the
root of a bbcode node tree.


```php

use PhpExtended\Bbcode\BbcodeParser;

$parser = new BbcodeParser()
$root = $parser->parse('<data>');
// $root is now a BbcodeCollectionNodeInterface

```


## License

MIT (See [license file](LICENSE)).
