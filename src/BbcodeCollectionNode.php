<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Bbcode;

/**
 * BbcodeCollectionNode class file.
 * 
 * This class is a simple implementation of the BbcodeCollectionNodeInterface.
 * 
 * @author Anastaszor
 */
class BbcodeCollectionNode extends BbcodeAbstractNode implements BbcodeCollectionNodeInterface
{
	
	/**
	 * The children of the node.
	 * 
	 * @var array<integer, BbcodeAbstractNodeInterface>
	 */
	protected array $_children = [];
	
	/**
	 * Builds a new BbcodeCollectionNode with the given name and children.
	 * 
	 * @param string $name
	 * @param string $property
	 * @param array<integer, BbcodeAbstractNodeInterface> $children
	 */
	public function __construct(string $name, ?string $property = null, array $children = [])
	{
		parent::__construct($name, $property);
		
		foreach($children as $child)
		{
			$this->addChild($child);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$data = [];
		
		foreach($this->_children as $node)
		{
			$data[] = $node->__toString();
		}
		
		$begin = $end = '';
		if(BbcodeAbstractNode::TYPE_TEXT !== $this->getName())
		{
			$begin = '['.$this->getName().(empty($this->getProperty()) ? '' : '="'.\str_replace('"', '\\"', $this->getProperty())).'"]';
			$end = '[/'.$this->getName().']';
		}
		
		return $begin.\implode('', $data).$end;
	}
	
	/**
	 * Adds the given child to the child list.
	 * 
	 * @param BbcodeAbstractNodeInterface $node
	 */
	public function addChild(BbcodeAbstractNodeInterface $node) : void
	{
		$this->_children[] = $node;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeCollectionNodeInterface::current()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function current() : BbcodeAbstractNodeInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return \current($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		\next($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return (int) \key($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return false !== \current($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		\reset($this->_children);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeCollectionNodeInterface::beVisitedBy()
	 */
	public function beVisitedBy(BbcodeVisitorInterface $visitor)
	{
		return $visitor->visitCollection($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeAbstractNode::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return empty($this->_children) && parent::isEmpty();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeAbstractNode::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof BbcodeCollectionNodeInterface
			&& $object->count() === $this->count()
			&& parent::equals($object);
	}
	
}
