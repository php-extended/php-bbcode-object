<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Bbcode;

/**
 * BbcodeTextNode class file.
 * 
 * This class is a simple implementation of the BbcodeSingleNodeInterface.
 * 
 * @author Anastaszor
 */
class BbcodeTextNode extends BbcodeAbstractNode implements BbcodeSingleNodeInterface
{
	
	/**
	 * The current value of the node.
	 * 
	 * @var string
	 */
	protected string $_text = '';
	
	/**
	 * Builds a new BbcodeTextNode with the given value.
	 * 
	 * @param ?string $value
	 */
	public function __construct(?string $value = null)
	{
		parent::__construct(BbcodeAbstractNode::TYPE_TEXT, null);
		$this->_text = (string) $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_text;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeSingleNodeInterface::getValue()
	 */
	public function getValue() : string
	{
		return $this->_text;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeSingleNodeInterface::isAutoclosed()
	 */
	public function isAutoclosed() : bool
	{
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeAbstractNodeInterface::beVisitedBy()
	 */
	public function beVisitedBy(BbcodeVisitorInterface $visitor)
	{
		return $visitor->visitSingle($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeAbstractNode::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return empty($this->_text) && parent::isEmpty();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeAbstractNode::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof BbcodeSingleNodeInterface
			&& $object->getValue() === $this->getValue()
			&& parent::equals($object);
	}
	
}
