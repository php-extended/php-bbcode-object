<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Bbcode;

/**
 * BbcodeAbstractNode class file.
 * 
 * This class is a simple implementation of the BbcodeAbstractNodeInterface.
 * 
 * @author Anastaszor
 */
abstract class BbcodeAbstractNode implements BbcodeAbstractNodeInterface
{
	
	public const TYPE_TEXT = 'text';
	
	/**
	 * The name of the node.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The property of the node, if any.
	 * 
	 * @var ?string
	 */
	protected ?string $_property = null;
	
	/**
	 * Builds a new node with the given name.
	 * 
	 * @param string $name
	 * @param ?string $property
	 */
	public function __construct(string $name, ?string $property = null)
	{
		$this->_name = $name;
		$this->_property = $property;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		if(empty($this->getProperty()))
		{
			return '['.$this->getName().']';
		}
		
		return '['.$this->getName().'='.$this->getProperty().']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeAbstractNodeInterface::getName()
	 */
	public function getName() : string
	{
		return (string) $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeAbstractNodeInterface::getProperty()
	 */
	public function getProperty() : string
	{
		return (string) $this->_property;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeAbstractNodeInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return empty($this->_name);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeAbstractNodeInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof BbcodeAbstractNodeInterface
			&& $object->getName() === $this->getName()
			&& $object->getProperty() === $this->getProperty();
	}
	
}
