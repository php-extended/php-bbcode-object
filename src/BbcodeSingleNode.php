<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Bbcode;

/**
 * BbcodeSingleNode class file.
 * 
 * This class is a simple implementation of the BbcodeSingleNodeInterface.
 * 
 * @author Anastaszor
 */
class BbcodeSingleNode extends BbcodeAbstractNode implements BbcodeSingleNodeInterface
{
	
	/**
	 * The value of the node.
	 * 
	 * @var ?string
	 */
	protected ?string $_value = null;
	
	/**
	 * Builds a new BbcodeSingleNode with the given name, value and autoclosed
	 * parameter.
	 * 
	 * @param string $name
	 * @param ?string $property
	 * @param ?string $value
	 */
	public function __construct(string $name, ?string $property = null, ?string $value = null)
	{
		parent::__construct($name, $property);
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$begin = '['.$this->getName().(empty($this->getProperty()) ? '' : '="'.\str_replace('"', '\\"', $this->getProperty()).'"').']';
		if(null === $this->_value || '' === (\trim($this->_value)))
		{
			return $begin;
		}
		
		return $begin.$this->getValue().'[/'.$this->getName().']';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeSingleNodeInterface::getValue()
	 */
	public function getValue() : string
	{
		return (string) $this->_value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeSingleNodeInterface::isAutoclosed()
	 */
	public function isAutoclosed() : bool
	{
		return null === $this->_value || '' === (\trim($this->_value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeSingleNodeInterface::beVisitedBy()
	 */
	public function beVisitedBy(BbcodeVisitorInterface $visitor)
	{
		return $visitor->visitSingle($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeAbstractNode::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return (null === $this->_value || '' === (\trim($this->_value))) && parent::isEmpty();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeAbstractNode::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof BbcodeSingleNodeInterface
			&& $object->getValue() === $this->getValue()
			&& parent::equals($object);
	}
	
}
