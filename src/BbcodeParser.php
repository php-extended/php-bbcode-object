<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-bbcode-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Bbcode;

use PhpExtended\Lexer\LexemeInterface;
use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;
use SplStack;

/**
 * BbcodeParser class file.
 * 
 * This class is a simple implementation of the BbcodeParserInterface based on 
 * a char lexer.
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<BbcodeCollectionNodeInterface>
 */
class BbcodeParser extends AbstractParserLexer implements BbcodeParserInterface
{
	
	public const L_CHR_STARTING = 1; // [
	public const L_CHR_SLASHING = 2; // /
	public const L_CHR_ENDING = 3; // ]
	public const L_CHR_ESCAPING = 4; // \
	public const L_CHR_EQUALS = 5; // =
	public const L_CHR_QUOTE = 6; // "
	public const L_TAG_BUILDING = 7; // [--
	public const L_TAG_VALUEING = 8; // [--=
	public const L_TAG_VALQTING = 9; // [--="
	public const L_TAG_VALQESCP = 10; // [--="--\
	public const L_TAG_COMPSIMP = 11; // [--]
	public const L_TAG_COMPVALU = 12; // [--=--]
	public const L_TAG_COMPQUOT = 13; // [--="--"]
	public const L_TAG_BUILDEND = 14; // [/
	public const L_TAG_VALUEEND = 15; // [/--
	public const L_TAG_COMPLEND = 16; // [/--]
	
	/**
	 * The tags that, if encountered, cannot have children. Default lists 
	 * include the [br], the [hr], and the [*] tag.
	 * 
	 * @var array<integer, string>
	 */
	public static array $_autoclosedTags = [
		'br',	// new line
		'hr',	// horizontal bar
		'*',	// item in list
	];
	
	/**
	 * Builds a new BbcodeParser with its given configuration.
	 */
	public function __construct()
	{
		parent::__construct(BbcodeCollectionNodeInterface::class);
		$this->_config->addMappings('[', self::L_CHR_STARTING);
		$this->_config->addMappings('/', self::L_CHR_SLASHING);
		$this->_config->addMappings(']', self::L_CHR_ENDING);
		$this->_config->addMappings('\\', self::L_CHR_ESCAPING);
		$this->_config->addMappings('=', self::L_CHR_EQUALS);
		$this->_config->addMappings('"', self::L_CHR_QUOTE);
		// everything else is junk (i.e. non significant data that we shall not lose)
		
		// concatenate every non special character inside a [ as a tag
		$this->_config->addMerging(self::L_CHR_STARTING, self::L_CHR_STARTING, self::L_CHR_STARTING); // [[ counts as 1
		$this->_config->addMerging(self::L_CHR_STARTING, LexerInterface::L_TRASH, self::L_TAG_BUILDING);
		$this->_config->addMerging(self::L_TAG_BUILDING, LexerInterface::L_TRASH, self::L_TAG_BUILDING);
		
		$this->_config->addMerging(self::L_CHR_STARTING, self::L_CHR_SLASHING, self::L_TAG_BUILDEND); // [/
		$this->_config->addMerging(self::L_TAG_BUILDING, self::L_CHR_SLASHING, self::L_TAG_BUILDEND); // // counts as 1
		$this->_config->addMerging(self::L_TAG_BUILDEND, LexerInterface::L_TRASH, self::L_TAG_VALUEEND);
		$this->_config->addMerging(self::L_TAG_VALUEEND, LexerInterface::L_TRASH, self::L_TAG_VALUEEND);
		
		// if a ] is encountered, end the tag
		$this->_config->addMerging(self::L_TAG_BUILDING, self::L_CHR_ENDING, self::L_TAG_COMPSIMP);
		$this->_config->addMerging(self::L_TAG_COMPSIMP, self::L_CHR_ENDING, self::L_TAG_COMPSIMP); // ]] counts as 1
		$this->_config->addMerging(self::L_TAG_BUILDEND, self::L_CHR_ENDING, self::L_TAG_COMPLEND);
		$this->_config->addMerging(self::L_TAG_VALUEEND, self::L_CHR_ENDING, self::L_TAG_COMPLEND);
		$this->_config->addMerging(self::L_TAG_COMPLEND, self::L_CHR_ENDING, self::L_TAG_COMPLEND);	// ]] counts as 1
		
		// if a = is encountered, the tag has a value
		$this->_config->addMerging(self::L_TAG_BUILDING, self::L_CHR_EQUALS, self::L_TAG_VALUEING);
		$this->_config->addMerging(self::L_TAG_VALUEING, LexerInterface::L_TRASH, self::L_TAG_VALUEING);
		$this->_config->addMerging(self::L_TAG_VALUEING, self::L_CHR_ENDING, self::L_TAG_COMPVALU);
		$this->_config->addMerging(self::L_TAG_COMPVALU, self::L_CHR_ENDING, self::L_TAG_COMPVALU); // ]] counts as 1
		
		// if a =" is encountered, the tag as a quoted value
		$this->_config->addMerging(self::L_TAG_VALUEING, self::L_CHR_QUOTE, self::L_TAG_VALQTING);
		$this->_config->addMerging(self::L_TAG_VALQTING, LexerInterface::L_TRASH, self::L_TAG_VALQTING);
		$this->_config->addMerging(self::L_TAG_VALQTING, self::L_CHR_STARTING, self::L_TAG_VALQTING);
		$this->_config->addMerging(self::L_TAG_VALQTING, self::L_CHR_ENDING, self::L_TAG_VALQTING);
		$this->_config->addMerging(self::L_TAG_VALQTING, self::L_CHR_ESCAPING, self::L_TAG_VALQTING);
		$this->_config->addMerging(self::L_TAG_VALQTING, self::L_CHR_EQUALS, self::L_TAG_VALQTING);
		$this->_config->addMerging(self::L_TAG_VALQTING, self::L_CHR_QUOTE, self::L_TAG_COMPQUOT);
		$this->_config->addMerging(self::L_TAG_COMPQUOT, self::L_CHR_ENDING, self::L_TAG_COMPQUOT); // ]] counts as 1
		
		// if a \ is encountered inside a quote, escape the next char
		$this->_config->addMerging(self::L_TAG_VALQTING, self::L_CHR_ESCAPING, self::L_TAG_VALQESCP);
		$this->_config->addMerging(self::L_TAG_VALQESCP, self::L_CHR_STARTING, self::L_TAG_VALQTING);
		$this->_config->addMerging(self::L_TAG_VALQESCP, self::L_CHR_ENDING, self::L_TAG_VALQTING);
		$this->_config->addMerging(self::L_TAG_VALQESCP, self::L_CHR_ESCAPING, self::L_TAG_VALQTING);
		$this->_config->addMerging(self::L_TAG_VALQESCP, self::L_CHR_EQUALS, self::L_TAG_VALQTING);
		$this->_config->addMerging(self::L_TAG_VALQESCP, self::L_CHR_QUOTE, self::L_TAG_VALQTING);
		$this->_config->addMerging(self::L_TAG_VALQESCP, LexerInterface::L_TRASH, self::L_TAG_VALQTING);
		
		// concatenate every character other than [ to trash
		$this->_config->addMerging(LexerInterface::L_TRASH, self::L_CHR_SLASHING, LexerInterface::L_TRASH);
		$this->_config->addMerging(LexerInterface::L_TRASH, self::L_CHR_ENDING, LexerInterface::L_TRASH);
		$this->_config->addMerging(LexerInterface::L_TRASH, self::L_CHR_ESCAPING, LexerInterface::L_TRASH);
		$this->_config->addMerging(LexerInterface::L_TRASH, self::L_CHR_EQUALS, LexerInterface::L_TRASH);
		$this->_config->addMerging(LexerInterface::L_TRASH, self::L_CHR_QUOTE, LexerInterface::L_TRASH);
	}
	
	/**
	 * Parses the data that provides from the given lexer.
	 * 
	 * @param LexerInterface $lexer
	 * @return BbcodeCollectionNodeInterface
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	public function parseLexer(LexerInterface $lexer) : BbcodeCollectionNodeInterface
	{
		// token stack of only collection nodes
		/** @var SplStack<BbcodeCollectionNode> $tokenStack */
		$tokenStack = new SplStack(); // use only push, pop and is empty
		$rootNode = new BbcodeCollectionNode(BbcodeCollectionNode::TYPE_TEXT);
		
		// build the token stack based on the parsing of the data
		$lexer->rewind();
		
		while($lexer->valid())
		{
			if($tokenStack->isEmpty())
			{
				$tokenStack->push($rootNode);
			}
			
			/** @var \PhpExtended\Lexer\Lexeme $token */
			$token = $lexer->current();
			
			switch($token->getCode())
			{
				case self::L_CHR_STARTING:
				case self::L_CHR_SLASHING:
				case self::L_CHR_ENDING:
				case self::L_CHR_ESCAPING:
				case self::L_CHR_EQUALS:
				case self::L_CHR_QUOTE:
					// single character
					$this->processSingleCharacterToken($tokenStack, $token);
					break;
					
				case self::L_TAG_BUILDING:
					// tag that is not finished
					$this->processTagBuilding($tokenStack, $token);
					break;
					
				case self::L_TAG_VALUEING:
					// tag that is not finished but with value
					$this->processTagValueing($tokenStack, $token);
					break;
					
				case self::L_TAG_VALQTING:
					// tag that is not finished but with quoted value
					$this->processTagValQuoting($tokenStack, $token);
					break;
					
				case self::L_TAG_VALQESCP:
					// tag that is not finished but with quoted and escaped value
					$this->processTagValQuotEscape($tokenStack, $token);
					break;
					
				case self::L_TAG_COMPSIMP:
					// begin tag that is correcly completed with single value
					$this->processTagCompSimp($tokenStack, $token);
					break;
					
				case self::L_TAG_COMPVALU:
					// begin tag that is correctly completed with value simple
					$this->processTagCompValue($tokenStack, $token);
					// no break
					
					// No break, cascades
				case self::L_TAG_COMPQUOT:
					// begin tag that is correctly completed with value with quote
					$this->processTagCompQuot($tokenStack, $token);
					break;
					
				case self::L_TAG_BUILDEND:
					// end tag that is not finished
					$tokenStack->pop();
					break;
					
				case self::L_TAG_VALUEEND:
					// end tag that is not finished
					$this->processTagValueEnd($rootNode, $tokenStack, $token);
					break;
					
				case self::L_TAG_COMPLEND:
					// end tag that is correctly completed
					$this->processTagCompleted($rootNode, $tokenStack, $token);
					break;
					
				default:
					// should not happen
					$child = new BbcodeTextNode($token->getData());
					$tokenStack->top()->addChild($child);
					break;
			}
			$lexer->next();
		}
		
		return $rootNode;
	}
	
	/**
	 * Process a single character token.
	 * 
	 * @param SplStack<BbcodeCollectionNode> $tokenStack
	 * @param LexemeInterface $token
	 */
	public function processSingleCharacterToken(SplStack $tokenStack, LexemeInterface $token) : void
	{
		$newNode = new BbcodeTextNode($token->getData());
		$tokenStack->top()->addChild($newNode);
	}
	
	/**
	 * Process a single tag building.
	 * 
	 * @param SplStack<BbcodeCollectionNode> $tokenStack
	 * @param LexemeInterface $token
	 */
	public function processTagBuilding(SplStack $tokenStack, LexemeInterface $token) : void
	{
		$name = \ltrim($token->getData(), '[');
		$this->pushNodeToStack($tokenStack, $name);
	}
	
	/**
	 * Process a single tag valueing token.
	 * 
	 * @param SplStack<BbcodeCollectionNode> $tokenStack
	 * @param LexemeInterface $token
	 */
	public function processTagValueing(SplStack $tokenStack, LexemeInterface $token) : void
	{
		$parts = \explode('=', $token->getData(), 2);
		$name = \ltrim($parts[0], '[');
		$param = $parts[1] ?? null;
		$this->pushNodeToStack($tokenStack, $name, $param);
	}
	
	/**
	 * Process a single tag val quoting.
	 * 
	 * @param SplStack<BbcodeCollectionNode> $tokenStack
	 * @param LexemeInterface $token
	 */
	public function processTagValQuoting(SplStack $tokenStack, LexemeInterface $token) : void
	{
		$parts = \explode('=', $token->getData(), 2);
		$name = \ltrim($parts[0], '[');
		$param = isset($parts[1]) ? \ltrim($parts[1], '"') : null;
		$this->pushNodeToStack($tokenStack, $name, $param);
	}
	
	/**
	 * Process a single tag val quote escape.
	 * 
	 * @param SplStack<BbcodeCollectionNode> $tokenStack
	 * @param LexemeInterface $token
	 */
	public function processTagValQuotEscape(SplStack $tokenStack, LexemeInterface $token) : void
	{
		$parts = \explode('=', $token->getData(), 2);
		$name = \ltrim($parts[0], '[');
		$param = isset($parts[1]) ? \rtrim(\rtrim(\rtrim($parts[1], ']'), '\\'), '"') : null;
		$this->pushNodeToStack($tokenStack, $name, $param);
	}
	
	/**
	 * Process a single comp simp value token.
	 * 
	 * @param SplStack<BbcodeCollectionNode> $tokenStack
	 * @param LexemeInterface $token
	 */
	public function processTagCompSimp(SplStack $tokenStack, LexemeInterface $token) : void
	{
		$name = \rtrim(\ltrim($token->getData(), '['), ']');
		$this->pushNodeToStack($tokenStack, $name);
	}
	
	/**
	 * Process a single tag comp value token.
	 * 
	 * @param SplStack<BbcodeCollectionNode> $tokenStack
	 * @param LexemeInterface $token
	 */
	public function processTagCompValue(SplStack $tokenStack, LexemeInterface $token) : void
	{
		$parts = \explode('=', $token->getData(), 2);
		$name = \ltrim($parts[0], '[');
		$param = isset($parts[1]) ? \trim(\rtrim($parts[1], ']'), '"') : null;
		$this->pushNodeToStack($tokenStack, $name, $param);
	}
	
	/**
	 * Processes a single tag comp quoted token.
	 * 
	 * @param SplStack<BbcodeCollectionNode> $tokenStack
	 * @param LexemeInterface $token
	 */
	public function processTagCompQuot(SplStack $tokenStack, LexemeInterface $token) : void
	{
		$parts = \explode('=', $token->getData(), 2);
		$name = \ltrim($parts[0], '[');
		$param = isset($parts[1]) ? \str_replace('\\"', '"', \trim(\rtrim($parts[1], ']'), '"')) : null;
		$this->pushNodeToStack($tokenStack, $name, $param);
	}
	
	/**
	 * @param SplStack<BbcodeCollectionNode> $tokenStack
	 * @param string $name
	 * @param ?string $param
	 */
	public function pushNodeToStack(SplStack $tokenStack, string $name, ?string $param = null) : void
	{
		$newNode = \in_array($name, self::$_autoclosedTags, true)
			? new BbcodeSingleNode($name, $param)
			: new BbcodeCollectionNode($name, $param);
		
		$tokenStack->top()->addChild($newNode);
		
		if($newNode instanceof BbcodeCollectionNode)
		{
			$tokenStack->push($newNode);
		}
	}
	
	/**
	 * Process a tag value ended.
	 * 
	 * @param BbcodeCollectionNode $rootNode
	 * @param SplStack<BbcodeCollectionNode> $tokenStack
	 * @param LexemeInterface $token
	 */
	public function processTagValueEnd(BbcodeCollectionNodeInterface $rootNode, SplStack $tokenStack, LexemeInterface $token) : void
	{
		/** @var BbcodeCollectionNode $last */
		$last = $tokenStack->top();
		$name = \ltrim(\ltrim($token->getData(), '['), '/');
		$tokenStack->pop();
		if($last->getName() !== $name)
		{
			$last = $tokenStack->isEmpty() ? $rootNode : $tokenStack->top();
			$last->addChild(new BbcodeTextNode($name));
		}
	}
	
	/**
	 * Process a tag completed.
	 * 
	 * @param BbcodeCollectionNode $rootNode
	 * @param SplStack<BbcodeCollectionNode> $tokenStack
	 * @param LexemeInterface $token
	 */
	public function processTagCompleted(BbcodeCollectionNode $rootNode, SplStack $tokenStack, LexemeInterface $token) : void
	{
		$name = \ltrim(\ltrim(\rtrim($token->getData(), ']'), '['), '/');
		$last = $tokenStack->top();
		$tokenStack->pop();
		if($last->getName() !== $name)
		{
			$last = $tokenStack->isEmpty() ? $rootNode : $tokenStack->top();
			$last->addChild(new BbcodeTextNode($name));
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Bbcode\BbcodeParserInterface::parseString()
	 */
	public function parse(?string $data) : BbcodeCollectionNodeInterface
	{
		return parent::parse($data); // interface compatibility
	}
	
}
